const gulp = require('gulp');
const umd = require('gulp-umd');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const karma = require('gulp-karma');
const babel = require('gulp-babel');
 
gulp.task('default', function() {
  return gulp.src('src/synce.js')
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(umd())
    .pipe(gulp.dest('dist'))
    .pipe(uglify())
    .pipe(rename('synce.min.js'))
    .pipe(gulp.dest('dist'));
});

gulp.task('test', ['default', 'karma']);

gulp.task('karma', function() {
  return gulp.src('./testsYo')
    .pipe(karma({
      configFile: 'karma.conf.js',
      action: 'run'
    }))
    .on('error', function(err) {
      console.log(err);
      this.emit('end');
    });  
});