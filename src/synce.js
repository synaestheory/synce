(function Synce() {
  function _passesNamingRequirements(name) {
    const rgx = /[^A-Za-z-]/g;

    if (typeof name !== "string") {
      throw new Error('Expected name to be of type string but received ' + typeof name);
      return false;
    }

    if (name.match(rgx)) {
      throw new Error('Name contains invalid characters');
      return false;
    }

    const indexOfHyphen = name.indexOf('-');

    if (indexOfHyphen === -1) {
      throw new Error('Name must contain at least one hyphen "-"');
      return false;
    }

    if (indexOfHyphen === 0 || indexOfHyphen === (name.length - 1)) {
      throw new Error('Name cannot start or end with a hyphen');
      return false;
    }

    return true;
  }


  function _modifyEvents(action) {
    if (this.events && typeof this.events === "object") {
      const keys = Object.keys(this.events)
      const fns = keys.map(k => this.events[k]);

      this._events = Object.create(this.events);
      this._eventKeys = Object.create(keys);
      this._eventFns = Object.create(fns);
      
      keys.forEach(k => {   
        const fn = this.events[k];
        const [eventType, ...tail] = k.split(' ');
        const querySelector = tail.join();        
        const els = querySelector.match(/[A-Z]/gi) ? this.$(querySelector) : [this];
        Array.from(els).forEach(el => el[action](eventType, this[fn].bind(this)));
      });
    }    
  }
  
  function _attachEvents() {
    _modifyEvents.call(this, 'addEventListener');
  }

  function _detachEvents() {
    _modifyEvents.call(this, 'removeEventListener');
  }

  function _update(target) {
    // function by Dr. Alex Rauschmayer - http://www.2ality.com/2012/08/underscore-extend.html
    const sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
      Object.getOwnPropertyNames(source).forEach(function(propName) {
        Object.defineProperty(target, propName,
          Object.getOwnPropertyDescriptor(source, propName)
        );
      });
    });

    return target;
  }

  function initialize() {
    const root = this.noShadow ? this : this.shadowRoot || this.createShadowRoot();
    Array.from(this.children).forEach(child => root.appendChild(document.adoptNode(child, true)));
    this.el = root;
    if (this.initialize && typeof this.initialize === 'function') {
      this.initialize();
    }

    return this;
  }

  function render() {
    // render innerHTML before delegating events
    if (this.render && typeof this.render === 'function') {
      this.render();
    }
    // TODO: add mutation observers for event delegation
    _attachEvents.call(this);

    return this;
  }

  function destroy() {

    if (this.destroy && typeof this.destroy === 'function') {
      this.destroy();
    }

    _detachEvents.call(this);

    return this;
  }

  function extend(name, proto) {
    
    if (_passesNamingRequirements(name)) {
      const prototype = _update(Object.create(Object.getPrototypeOf(this)), this, proto);
      document.registerElement(name, {prototype});

      return prototype;
    }

    // this should only return if the element registration above failed
    return this;
  }

  function fireChangeEvent(attrName, oldVal, newVal) {
    const event = new CustomEvent('change', {
      detail: {
        attrName,
        oldVal,
        val: newVal
      }
    });

    this.dispatchEvent(event);
  }

  function $(query) {
    return this.el.querySelectorAll(query);
  }

  const SynceElement = Object.create(HTMLElement.prototype);

  Object.defineProperties(SynceElement, {
    // alias val to value
    val: {
      get: function() {

        return this.value;
      },
      set: function(val) {

        return this.value = val;
      }
    }
  });

  SynceElement.$ = $;
  SynceElement.extend = extend;
  SynceElement.createdCallback = initialize;
  SynceElement.attachedCallback = render;
  SynceElement.detachedCallback = destroy;
  SynceElement.attributeChangedCallback = fireChangeEvent;
  
  const Element = Object.freeze(SynceElement);
  
  if (window.Synce === undefined) {
    window.Synce = {Element};
  }
  
  return {Element};
}());