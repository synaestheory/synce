describe('Synce', function() {
  
  it('should exist', function() {
    expect(window.Synce).not.toBe(undefined);
  });

  describe('Synce.Element', function() {  
    it('should exist', function() {
      expect(Synce.Element).not.toBe(undefined);
    });

    it('should have an "extend" method', function() {
      expect(typeof Synce.Element.extend).toBe('function');  
    });

    it('should throw an error if no parameters are passed into "extend"', function() {
      expect(() => Synce.Element.extend()).toThrow();
    });

    it('should throw an error if the name provided to extend does not meet naming standards', function() {
      expect(() => Synce.Element.extend('NameWithoutHyphen', {})).toThrow();
      expect(() => Synce.Element.extend('-hyphenAtStartOfName', {})).toThrow();
      expect(() => Synce.Element.extend('HyphenAtEndOfName-', {})).toThrow();
    });
    
    it('should register a custom element', function() {
      const testProto = Synce.Element.extend('test-element', {});
      expect(typeof testProto).toBe('object');
    });

    it('should not allow the same element to be registered twice', function() {
      expect(() => Synce.Element.extend('test-element', {})).toThrow();
    });
    
    it('should place default functions on each of the element lifecycle callbacks', function() {
      const testElement = document.createElement('test-element');
      
      expect(typeof testElement.createdCallback).toBe('function');
      expect(typeof testElement.attachedCallback).toBe('function');
      expect(typeof testElement.detachedCallback).toBe('function');
      expect(typeof testElement.attributeChangedCallback).toBe('function');
    });
    
    it('should allow for the disabling of shadowRoot on an element', function() {
      Synce.Element.extend('test-element-no-shadow', {noShadow: true});
      const testElement = document.createElement('test-element');
      const testElementNoShadow = document.createElement('test-element-no-shadow');
      
      expect(testElementNoShadow.shadowRoot).toBeFalsy();
      expect(testElement.shadowRoot).toBeTruthy();
    });
    
    it('should provide a common interface to the root of the element', function() {
      const testElement = document.createElement('test-element');
      const testElementNoShadow = document.createElement('test-element-no-shadow');
      
      expect(testElement.el).toEqual(testElement.shadowRoot);
      expect(testElementNoShadow.el).toEqual(testElementNoShadow);
      
    });
    
    it('should allow extending multiple elements', function() {
      const testExtendOne = Synce.Element.extend('test-extend-one', { property: 1 });
      const testExtendTwo = testExtendOne.extend('test-extend-two', { anotherProperty: 2 });
      const testExtendThree = testExtendTwo.extend('test-extend-three', { property: 3 });
      
      expect(testExtendOne.property).toBe(1);
      expect(testExtendTwo.property).toBe(1);
      expect(testExtendThree.property).toBe(3);
      expect(testExtendThree.anotherProperty).toBe(2);
    });
    
    it('should fire initialize on element creation', function() {
      const proto = Synce.Element.extend('test-initialize', { 
        initialize: function() {
          this.init = true;
        }
      });
      const element = document.createElement('test-initialize');
      
      expect(proto.init).toBe(undefined);
      expect(element.init).toBe(true);
      
    });
    
    it('should fire render method during attachedCallback', function() {
      const proto = Synce.Element.extend('test-render', { 
        render: function() {
          this.attached = true;
        }
      });
      const element = document.createElement('test-render');
        
      expect(proto.attached).toBe(undefined);
      expect(element.attached).toBe(undefined);
      
      document.body.appendChild(element);
      expect(proto.attached).toBe(undefined);
      expect(element.attached).toBe(true);
    });
    
    it('should call destroy method during detachedCallback', function() {
      const proto = Synce.Element.extend('test-destroy', {
        destroy: function() {
          this.detached = true;
        }
      });
      
      const element = document.createElement('test-destroy');
      
      document.body.appendChild(element);
      expect(proto.detached).toBe(undefined);
      expect(element.detached).toBe(undefined);
      element.remove();
      expect(proto.detached).toBe(undefined);
      expect(element.detached).toBe(true);
    });
    
    it('should enumerate events on the custom element', function() {
      const proto = Synce.Element.extend('test-event-bind', {
        events: {
          'click div': 'clickHandler'
        },
        initialize() {
          this.el.appendChild(document.createElement('div'));
        },
        clickHandler() {
          this.clicked = true;
        }
      });
      const element = document.createElement('test-event-bind');
      
      document.body.appendChild(element);
      expect(element.clicked).toBe(undefined);
      element.el.querySelector('div').click();
      expect(element.clicked).toBe(true);
    });
    
    it('should bind an event without a selector to the root of the element', function() {
      Synce.Element.extend('test-event-root', {
        events: {'click': 'clickHandler'},
        clickHandler() {
          this.clicked = true;
        }
      });
      const element = document.createElement('test-event-root');
      
      document.body.appendChild(element);
      expect(element.clicked).toBe(undefined);
      element.click();
      expect(element.clicked).toBe(true);
    });
  });
});
