# Synce
A small Custom Elements wrapper that mimics Backbone

The purpose of this micro-library is to create an interface around the new HTML Custom Element spec that is familiar to anyone who has used Backbone in one of their projects.

You create an element by simply calling 
`Synce.Element.extend(name, prototype);`

This registers a custom element and sets up the appropriate lifecycle callbacks for creation, attachment to the DOM, detachment from the DOM and an attribute changed listener.

## Getting Started
You can install Synce directly through npm 
`npm install synce`

then include either `synce.js` or `dist/synce.min.js` in your project.

## Polyfills
As the Custom Element spec has not been finalized, browser support is limited and I have specifically left the polyfills out of the built code.  There are some polyfills included as part of the dependencies in the package.json of this library which seem to do the job well, but I excluded them from the build process in case you wanted to use other polyfills or none at all if you're targeting Chrome ;-)

Most likely, as of time of this writing (8/4/2015), you will want at least a document.registerElement polyfill.  If you choose to use ShadowDOM (default setting in Synce), then you will also want a polyfill for that.

## Example
HTML
```html
<custom-element></custom-element>
```

JS
```javascript
Synce.Element.extend('custom-element', {
  events: {
    'click .worldly-button' : 'greet',
    'click' : 'greet' // leaving the query selector blank binds to the root of the custom element
  },
  noShadow: true,  // This disables the creation of a Shadow DOM at the root of the element
  greeting: 'Hello, world',
  initialize: function() {
    // this.el references the root of the element (Element root if ShadowDOM is disabled, otherwise ShadowRoot)
    this.el.innerHTML = '<button class="worldly-button" name="Hello">Synce</button>' 
    console.log('custom-element created');
  },
  render: function() {
    console.log('custom-element attached to the DOM');
  },
  destroy: function() {
    console.log('custom-element detached from the DOM');
  },
  greet: function(e) {
    e.preventDefault();
    console.log(this.greeting);
  }
});
```
## Synce.Element helpers

Synce.Element (which is the base element you extend from) includes two helpers to give you quick access to the shadowDOM if you have it enabled (`Synce.Element.el` && `Synce.Element.$`). 

`Synce.Element.el` points to the root of the custom element.  Unless shadowDOM was disabled using the noShadow property on the element prototype, it will reference the shadowRoot, otherwise it will reference the element root.

`Synce.Element.$` is shorthand for `Synce.Element.el.querySelectorAll`.  These are simply two convenience options that were built in to provide a standard interface into the root of the element whether or not it has shadowDOM.




