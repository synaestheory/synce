;(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory();
  } else {
    root.Synce = factory();
  }
}(this, function() {
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

function _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }

(function Synce() {
  function _passesNamingRequirements(name) {
    var rgx = /[^A-Za-z-]/g;

    if (typeof name !== "string") {
      throw new Error('Expected name to be of type string but received ' + (typeof name === 'undefined' ? 'undefined' : _typeof(name)));
      return false;
    }

    if (name.match(rgx)) {
      throw new Error('Name contains invalid characters');
      return false;
    }

    var indexOfHyphen = name.indexOf('-');

    if (indexOfHyphen === -1) {
      throw new Error('Name must contain at least one hyphen "-"');
      return false;
    }

    if (indexOfHyphen === 0 || indexOfHyphen === name.length - 1) {
      throw new Error('Name cannot start or end with a hyphen');
      return false;
    }

    return true;
  }

  function _modifyEvents(action) {
    var _this = this;

    if (this.events && _typeof(this.events) === "object") {
      var keys = Object.keys(this.events);
      var fns = keys.map(function (k) {
        return _this.events[k];
      });

      this._events = Object.create(this.events);
      this._eventKeys = Object.create(keys);
      this._eventFns = Object.create(fns);

      keys.forEach(function (k) {
        var fn = _this.events[k];

        var _k$split = k.split(' ');

        var _k$split2 = _toArray(_k$split);

        var eventType = _k$split2[0];

        var tail = _k$split2.slice(1);

        var querySelector = tail.join();
        var els = querySelector.match(/[A-Z]/gi) ? _this.$(querySelector) : [_this];
        Array.from(els).forEach(function (el) {
          return el[action](eventType, _this[fn].bind(_this));
        });
      });
    }
  }

  function _attachEvents() {
    _modifyEvents.call(this, 'addEventListener');
  }

  function _detachEvents() {
    _modifyEvents.call(this, 'removeEventListener');
  }

  function _update(target) {
    // function by Dr. Alex Rauschmayer - http://www.2ality.com/2012/08/underscore-extend.html
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
      Object.getOwnPropertyNames(source).forEach(function (propName) {
        Object.defineProperty(target, propName, Object.getOwnPropertyDescriptor(source, propName));
      });
    });

    return target;
  }

  function initialize() {
    var root = this.noShadow ? this : this.shadowRoot || this.createShadowRoot();
    Array.from(this.children).forEach(function (child) {
      return root.appendChild(document.adoptNode(child, true));
    });
    this.el = root;
    if (this.initialize && typeof this.initialize === 'function') {
      this.initialize();
    }

    return this;
  }

  function render() {
    // render innerHTML before delegating events
    if (this.render && typeof this.render === 'function') {
      this.render();
    }
    // TODO: add mutation observers for event delegation
    _attachEvents.call(this);

    return this;
  }

  function destroy() {

    if (this.destroy && typeof this.destroy === 'function') {
      this.destroy();
    }

    _detachEvents.call(this);

    return this;
  }

  function extend(name, proto) {

    if (_passesNamingRequirements(name)) {
      var prototype = _update(Object.create(Object.getPrototypeOf(this)), this, proto);
      document.registerElement(name, { prototype: prototype });

      return prototype;
    }

    // this should only return if the element registration above failed
    return this;
  }

  function fireChangeEvent(attrName, oldVal, newVal) {
    var event = new CustomEvent('change', {
      detail: {
        attrName: attrName,
        oldVal: oldVal,
        val: newVal
      }
    });

    this.dispatchEvent(event);
  }

  function $(query) {
    return this.el.querySelectorAll(query);
  }

  var SynceElement = Object.create(HTMLElement.prototype);

  Object.defineProperties(SynceElement, {
    // alias val to value
    val: {
      get: function get() {

        return this.value;
      },
      set: function set(val) {

        return this.value = val;
      }
    }
  });

  SynceElement.$ = $;
  SynceElement.extend = extend;
  SynceElement.createdCallback = initialize;
  SynceElement.attachedCallback = render;
  SynceElement.detachedCallback = destroy;
  SynceElement.attributeChangedCallback = fireChangeEvent;

  var Element = Object.freeze(SynceElement);

  if (window.Synce === undefined) {
    window.Synce = { Element: Element };
  }

  return { Element: Element };
})();
return Synce;
}));
