## [2.0.0]
### Changes
* Updated more code to ES6
### Breaking Changes
* Removed `super` method
